<?php
if (preg_match('/\.(?:png|jpg|jpeg|gif|css|js)$/', $_SERVER["REQUEST_URI"])) {
    return false;
}

$rota = parse_url('http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);
$url = explode('/',ltrim($rota['path'],'/'));

$request = array('pagina'=>'home');
if (isset($_GET) && isset($_GET['pagina'])) {
    $request = filter_input_array(INPUT_GET,$_GET);
}

$load = function () use ($request)
{
    $pasta = 'includes';
    $include = '404';
    foreach ($request as $arquivo) {
        if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $pasta . DIRECTORY_SEPARATOR . $arquivo . '.php')
            && !is_dir(__DIR__ . DIRECTORY_SEPARATOR . $pasta . DIRECTORY_SEPARATOR . $arquivo . '.php')
        ) {
            $include = $arquivo;
        } else {
            header('HTTP/1.0 404 Not Found');
            $include = '404';
        }
    }
    include_once(__DIR__ . DIRECTORY_SEPARATOR . $pasta . DIRECTORY_SEPARATOR . $include . '.php');
};

function getIpUsuario(){
    switch(true){
        case (!empty($_SERVER['HTTP_X_REAL_IP'])) : return $_SERVER['HTTP_X_REAL_IP'];
        case (!empty($_SERVER['HTTP_CLIENT_IP'])) : return $_SERVER['HTTP_CLIENT_IP'];
        case (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) : return $_SERVER['HTTP_X_FORWARDED_FOR'];
        default : return $_SERVER['REMOTE_ADDR'];
    }
};