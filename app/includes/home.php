<?php
require_once 'app/database/db.php';
$mensagens = $getVisitas();
$vazio = array('nome' => 'Ninguém',
    'mensagem' => 'Nenhuma mensagem gravada.',
    'data_hora' => '');
$ultimaMensagem = (count($mensagens) > 0 ? $mensagens[0] : $vazio);
?>

<section class="painel-topo">
    <h1 class="texto-branco">O Pai da Computação</h1>

    <p class="destaque texto-branco">"Nós enchergamos apenas a uma curta distância a frente, mas podemos ver que existe
        muito a ser feito."
        <br><span class="pull-right">- Alan Mathison Turing</span>
    </p>
</section>
<section class="conteudo-interno">
    <div class="linha">
        <div class="coluna col-75">
            <article>
                <h4 class="legend">Sobre</h4>

                <p class="destaque">Alan Mathison Turing OBE (23 de Junho de 1912 — 7 de Junho de 1954) foi um
                    matemático,
                    lógico,
                    criptoanalista e cientista da computação britânico. Foi influente no desenvolvimento da ciência da
                    computação e na formalização do conceito de algoritmo e computação com a máquina de Turing,
                    desempenhando um papel importante na criação do computador moderno.Ele também é pioneiro na
                    inteligência artificial e na ciência da computação.</p>
            </article>

            <article>
                <h4 class="legend">Bomba Eletromecânica</h4>

                <p>Durante a Segunda Guerra Mundial, Turing trabalhou para a inteligência britânica em Bletchley Park,
                    num
                    centro especializado em quebra de códigos. Por um tempo ele foi chefe do Hut 8, a seção responsável
                    pela
                    criptoanálise da frota naval alemã. Planejou uma série de técnicas para quebrar os códigos alemães,
                    incluindo o método da bomba eletromecânica, uma máquina eletromecânica que poderia encontrar
                    definições
                    para
                    a máquina Enigma.</p>

                <figure class="foto-legenda">
                    <img src="img/bomba.jpg"/>
                    <figcaption class="texto-branco">
                        <h3 class="texto-branco">Bomba Eletromecânica</h3>

                        <p id="pFoto">
                            Dispositivo de análise criptográfica, que podia decifrar as mensagens
                            codificadas da célebre máquina alemã Enigma, utilizada pelos nazistas para dirigir as suas
                            operações navais.
                        </p>
                    </figcaption>
                </figure>

                <p>Após a guerra, trabalhou no Laboratório Nacional de Física do Reino Unido, onde criou um dos
                    primeiros
                    projetos para um computador com um programa armazenado, o ACE. Posteriormente, Turing se interessou
                    pela
                    química. Escreveu um artigo sobre a base química da morfogênese e previu reações químicas oscilantes
                    como a
                    Reação de Belousov-Zhabotinsky, que foram observadas pela primeira vez na década de 1960.</p>

            </article>

            <article>
                <h4 class="legend">5 Descobertas</h4>
                <table>
                    <thead>
                    <tr>
                        <th>Ano</th>
                        <th>Relização</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr class="odd">
                        <td>1936</td>
                        <td>A Máquina de Turing</td>
                    </tr>
                    <tr>
                        <td>1943</td>
                        <td>A solução da Enigma</td>
                    </tr>
                    <tr class="odd">
                        <td>1944</td>
                        <td>Criptografia de voz</td>
                    </tr>
                    <tr>
                        <td>1947</td>
                        <td class="cd">O Teste de Turing</td>
                    </tr>
                    <tr class="odd">
                        <td>1950</td>
                        <td>O Computador ACE</td>
                    </tr>
                    </tbody>
                </table>

            </article>

            <article>

                <h4 class="legend">Homosexualidade</h4>

                <p>A homossexualidade de Turing resultou em um processo criminal em 1952 - os atos homossexuais eram
                    ilegais
                    no
                    Reino Unido na época, e ele aceitou o tratamento com hormônios femininos e castração química, como
                    alternativa à prisão. Morreu em 1954, algumas semanas antes de seu aniversário de 42 anos, devido a
                    um
                    aparente auto-administrado envenenamento por cianeto, apesar de sua mãe (e alguns outros) ter
                    considerado a
                    sua morte acidental. Em 10 de setembro de 2009, após uma campanha de internet, o primeiro-ministro
                    britânico
                    Gordon Brown fez um pedido oficial de desculpas público, em nome do governo britânico, devido à
                    maneira
                    pela
                    qual Turing foi tratado após a guerra. Em 24 de dezembro de 2013, Alan Turing recebeu o perdão real
                    da
                    rainha Elisabete II, da condenação por homossexualidade.
                </p>
            </article>

            <article>
                <h4 class="legend">Cinebiografias</h4>

                <p>
                    Parte de sua vida foi retratada no telefilme Breaking the Code, de 1996, com o ator Derek Jacobi no
                    papel principal.
                </p>

                <p>
                    Em 2014 sua vida foi tratada no filme "The Imitation Game" (lançado como 'O Jogo da Imitação' no
                    Brasil). A obra tem como focos o seu trabalho
                    decisivo para a vitória dos Aliados na Segunda Guerra Mundial e sua contribuição fundamental para o
                    desenvolvimento da informática. "The Imitation Game" foi lançado nos Estados Unidos em 28 de
                    Novembro de
                    2014 e no Brasil em 22 de Janeiro de 2015. O filme é dirigido por Morten Tyldum, um cineasta
                    norueguês
                    que trabalhou pela primeira vez com um filme em inglês. Turing foi representado pelo ator britânico
                    Benedict Cumberbatch e o elenco conta ainda com Keira Knightley, Matthew Goode e Charles Dance.
                </p>
                <iframe class="youtube-video" frameborder="0"
                        src="https://www.youtube.com/embed/YIkKbMcJL_4"
                        allowfullscreen>
                </iframe>
            </article>
        </div>

        <aside class="coluna col-25">
            <div class="lateral">
                <h4 class="legend">Alan Mathison Turing</h4>
                <img src="img/alan_turing.jpg">

                <p>
                    Britânico, nascido no Reino Unido em 23 de junho de 1912.
                </p>

                <p>
                    Falecido em 7 de Junho de 1954 (41 anos).
                </p>

                <h4 class="legend">Mensagens</h4>

                <div>
                    <i class="fa fa-user"></i> <?php echo $ultimaMensagem['nome']; ?>
                </div>

                <div title="<?php echo $ultimaMensagem['data_hora']; ?>">
                    <i class="fa fa-comment-o"></i> <?php echo $mensagem = strtr($ultimaMensagem['mensagem'], $emoticons); ?>
                </div>
                <p><a class="botao botao-verde" href="?pagina=guestbook#nova">Nova Mensagem!</a></p>
            </div>
        </aside>
    </div>
</section>