<div class="painel-topo">
    <div class="conteudo">
        <h1><span class="glyphicon glyphicon-warning-sign"></span> Erro 404</h1>

        <p>O endereço solicitado não existe.</p>
        <p><a href="?go=home" class="botao botao-vermelho botao-grande" role="button">
                <span class="glyphicon glyphicon-circle-arrow-left"></span> Voltar para Home</a></p>
    </div>
</div>