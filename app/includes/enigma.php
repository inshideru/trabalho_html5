<section class="titulo">
    <h1 class="legend">A Máquina Enigma</h1>
</section>

<section class="conteudo-interno">
    <div class="linha">
        <div class="coluna col-100">

            <p>
                Enigma é o nome por que é conhecida uma máquina electro-mecânica de criptografia com rotores, utilizada
                tanto para criptografar como para descriptografar mensagens secretas, usada em várias formas na Europa a
                partir dos anos 1920. A sua fama vem de ter sido adaptada pela maior parte das forças militares alemãs a
                partir de cerca de 1930. A facilidade de uso e a suposta indecifrabilidade do código foram as principais
                razões para a sua popularidade. O código foi, no entanto, decifrado, e a informação contida nas
                mensagens que ele não protegeu é geralmente tida como responsável pelo fim da Segunda Guerra Mundial
                pelo menos um ano antes do que seria de prever.
            </p>

            <ul id="album-fotos">
                <li
                    id="foto1">
                    <span>Máquina Enigma</span>
                </li>
                <li id="foto2">
                    <span>Vista lateral do Rotor</span>
                </li>
                <li id="foto3">
                    <span>Máquina Enigma</span>
                </li>
                <li id="foto4">
                    <span>Partes Internas</span>
                </li>
                <li id="foto5">
                    <span>Plugs</span>
                </li>
                <li id="foto6">
                    <span>Teclas</span>
                </li>
            </ul>
        </div>
        <div class="coluna col-100">
            <h4>Funcionamento</h4>

            <small>
                Tal como outras máquinas com rotores, a Máquina Enigma é uma combinação de sistemas mecânicos e
                elétricos. O mecanismo consiste num teclado, num conjunto de discos rotativos chamados rotores,
                dispostos em fila; e de um mecanismo de avanço que faz andar alguns rotores uma posição quando uma tecla
                é pressionada. O mecanismo varia entre diversas versões da máquina, mas o mais comum é o rotor colocado
                à direita avançar uma posição com cada tecla premida, e ocasionalmente despoletar o movimento rotativo
                dos restantes rotores, à sua esquerda, à semelhança do mecanismo conta-quilómetros de um automóvel. O
                movimento contínuo dos rotores provoca diferentes combinações na criptografia.
            </small>
        </div>
    </div>
</section>