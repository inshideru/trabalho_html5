<?php
require_once 'app/database/db.php';
?>
<script src="js/guestbook.js"></script>
<section class="titulo">
    <h1 class="legend">Livro de Visitas</h1>
</section>

<section class="conteudo-interno">
    <div class="linha">

        <div class="coluna col-100">
            <div id="gb-mensagens">
                <?php if (isset($_POST['enviar'])) : ?>
                    <div class="message-box" id="visit-msg">
                        <ul class="adm-msg">
                            <a href="#" title="Fechar!" class="remove" id="remove-visit-msg">
                                <li><i class="fa fa-remove"></i></li>
                            </a>
                        </ul>
                        <p>
                            <i class="fa fa-arrow-circle-right"></i> <?php echo $gravarVisita($_POST); ?>
                        </p>
                    </div>
                <?php endif; ?>
                <h4 class="legend"><i class="fa fa-comment-o"></i> Mensagens
                    <ul class="adm-msg">
                        <a href="#nova" class="botao botao-azul">
                            <li><i class="fa fa-plus"></i> Nova</li>
                        </a>
                    </ul>
                </h4>

                <?php
                foreach ($getVisitas() as $key => $visita) {
                    $class = ($key % 2 == 0 ? 'even' : 'odd');
                    $mensagem = strtr($visita['mensagem'], $emoticons);
                    echo "<div class=\"bloco-mensagem {$class}\">
                        <div class=\"nome-visitante\">
                            <span class=\"fa-stack\">
                                <i class=\"fa fa-circle fa-stack-2x\"></i>
                                <i class=\"fa fa-user fa-stack-1x fa-inverse\"></i>
                            </span> {$visita['nome']}
                            <ul class=\"adm-msg\">
                                <a href=\"#nova\"
                                data-id-update=\"{$visita['codigo']}\"
                                data-nome-message=\"{$visita['nome']}\"
                                data-text-message=\"{$visita['mensagem']}\"
                                title=\"Atualizar\"
                                class=\"botao botao-cinza botao-update\"><li><i class=\"fa fa-refresh\"></i></li></a>
                                <a href=\"#nova\"
                                data-id-delete=\"{$visita['codigo']}\"
                                data-nome-message=\"{$visita['nome']}\"
                                data-text-message=\"{$visita['mensagem']}\"
                                title=\"Apagar\"
                                class=\"botao botao-cinza botao-delete\"><li><i class=\"fa fa-trash\"></i></li></a>
                            </ul>
                        </div>
                        <div class=\"text-mensagem\">{$mensagem}</div>
                        <div class=\"data-hora-mensagem\">
                            <i class=\"fa fa-clock-o\"></i> {$visita['data_hora']}
                        </div>
                      </div>";
                }
                ?>
            </div>
        </div>

        <div class="coluna col-100">
            <a name="nova">
                <h4 class="legend" id="form-msg-titulo"><i class="fa fa-plus-circle"></i> Nova Mensagem</h4>
            </a>
            <span id="confirm-delete" class="oculto"></span>

            <form method="post" class="form-horizontal" action="?pagina=guestbook">
                <fieldset>
                    <div class="form-group hide-on-delete">
                        <label for="nome" class="coluna col-25 form-label">Nome</label>

                        <div class="coluna col-75">
                            <input type="text" name="nome" class="form-control" id="nome"
                                   placeholder="Nome" maxlength="30"
                                   required="required">
                        </div>
                    </div>
                    <div class="form-group hide-on-delete">
                        <label for="mensagem" class="coluna col-25 form-label">Mensagem</label>

                        <div class="coluna col-75">
                                <textarea class="form-control" name="mensagem" id="mensagem" placeholder="Sua mensagem"
                                          rows="5"
                                          maxlength="255"
                                          required="required"></textarea>
                        </div>
                    </div>
                    <div class="clearfix pull-right">
                        <a href="?pagina=guestbook" class="botao botao-branco">
                            <i class="fa fa-arrow-circle-left"></i> Cancelar
                        </a>
                        <button type="submit" name="enviar" class="botao botao-branco">
                            <i class="fa fa-check"></i> Ok
                        </button>
                        <input type="hidden" name="codigo" id="codigo" value="">
                        <input type="hidden" name="operacao" id="operacao" value="I">
                    </div>

                </fieldset>
            </form>
            <div id="emoticons">
                <table>
                    <?php
                    foreach ($emoticons as $atalho => $imagem) {

                        $emo = "<button data-atalho=\"{$atalho}\" title=\"{$atalho}\" class=\"botao botao-branco atalho_emoticon\">{$imagem}</button>";

                        echo $emo;
                    }
                    ?>
                </table>
            </div>

        </div>
    </div>
</section>