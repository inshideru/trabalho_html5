<script src="js/timeline.js"></script>
<section class="titulo">
    <h1 class="legend">Linha do Tempo</h1>
</section>

<section class="conteudo-interno">

    <div class="linha">

        <div id="timeline-img">
            <img src="img/turing-timeline.jpg" usemap="#timeline-map" id="img-timeline">
            <map name="timeline-map">
                <area shape="rect" coords="5,5,125,45"
                      href="#" id="1936"/>
                <area shape="rect" coords="90,80,205,125"
                      href="#" id="1943"/>
                <area shape="rect" coords="145,5,265,45"
                      href="#" id="1944"/>
                <area shape="rect" coords="240,80,335,125"
                      href="#" id="1947"/>
                <area shape="rect" coords="295,5,398,45"
                      href="#" id="1950"/>
            </map>
        </div>
        <div id="col-iframe-timeline">
            <img src="" id="img-time-item">
            <div id="timeline-conteudo"></div>
        </div>
    </div>
</section>