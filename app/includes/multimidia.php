<section class="titulo">
    <h1 class="legend">Multimídia</h1>
</section>

<section class="conteudo-interno">
    <div class="linha">
        <div class="coluna col-100">
            <h4 class="legend">Demonstração da Máquina Enigma - Museu da UFRGS</h4>
        </div>
        <div class="coluna col-100 text-centro">
            <video
                id="filme"
                controls
                poster="img/multimidia/enigma_demostracao.jpg">
                <source
                    src="img/multimidia/Demonstração_Máquina_Enigma_-_Museu_da_UFRGS.mp4"
                    type="video/mp4"/>
            </video>
            <br>
            <br>
        </div>
        <div class="coluna col-100">
            <h4 class="legend">Narração - Resumo da Vida e Obra de Alan Turing</h4>
        </div>
        <div class="coluna col-100 text-centro">
            <audio id="musica" controls>

                <source src="img/multimidia/Alan-Turing_-_Resumo_Vida_e_Obra.mp3"
                        type="audio/mpeg"/>
                Desculpe, mas não foi possível carregar o áudio
            </audio>
            <br>
        </div>
    </div>
</section>