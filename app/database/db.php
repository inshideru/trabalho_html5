<?php
$pdo = null;
try {
    $pdo = new PDO('pgsql:host=localhost;dbname=guestbook', 'postgres', '123456');
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

} catch (PDOException $e) {
    echo $e->getMessage();
}

$getVisitas = function () use($pdo)
{
    $stmt = $pdo->prepare('SELECT * FROM vw_visita ORDER BY 1 DESC');
    $stmt->execute();
    return $stmt->fetchAll(PDO::FETCH_ASSOC);
};

$gravarVisita = function ($dados) use ($pdo)
{
    $codigo = ($_POST['codigo'] == '' ? 'NULL' : (int)$_POST['codigo']);
    $nome = ($_POST['nome'] == '' ? NULL : filter_var(trim(strip_tags($_POST['nome'])), FILTER_SANITIZE_STRING));
    $mensagem = ($_POST['mensagem'] == '' ? NULL : filter_var(trim(strip_tags($_POST['mensagem'])), FILTER_SANITIZE_STRING));
    $ip = getIpUsuario();
    $operacao = ($_POST['operacao'] == '' ? NULL : filter_var($_POST['operacao'], FILTER_SANITIZE_STRING));

    $query = $pdo->prepare("
        SELECT fn_visita({$codigo}, '{$nome}', '{$mensagem}', '{$ip}', '{$operacao}')
    ");
    $query->execute();

    return $query->fetch(PDO::FETCH_ASSOC)['fn_visita'];
};

$emoticons = array(
    ':)' => '<img src="img/emoticons/smile.gif">',
    ':D' => '<img src="img/emoticons/grin.gif">',
    ':love:' => '<img src="img/emoticons/love.gif">',
    'u__u' => '<img src="img/emoticons/blush.gif">',
    '8)' => '<img src="img/emoticons/c8.gif">',
    ':(' => '<img src="img/emoticons/sad.gif">',
    '>:(' => '<img src="img/emoticons/angry.gif">',
    '+__+' => '<img src="img/emoticons/dead.gif">',
    'B)' => '<img src="img/emoticons/cool.gif">',
    ';_;' => '<img src="img/emoticons/cry.gif">',
    'zzz' => '<img src="img/emoticons/bored.gif">',
    'O__O' => '<img src="img/emoticons/shocked.gif">',
    ';)' => '<img src="img/emoticons/wink.gif">',
    ':O' => '<img src="img/emoticons/oh_god.gif">',
    '>:)' => '<img src="img/emoticons/evil.gif">',
    'café' => '<img src="img/emoticons/coffee.gif">',
);