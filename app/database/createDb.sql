/*
CREATE DATABASE guestbook
TEMPLATE TEMPLATE0
ENCODING 'UTF-8'
CONNECTION LIMIT 100;
*/

/* Criação da Tabela */
CREATE TABLE tb_visita (
  codigo    SERIAL,
  nome      VARCHAR(30)  CONSTRAINT nn_nome_tb_visita NOT NULL,
  mensagem  VARCHAR(255) CONSTRAINT nn_mensagens_tb_visita NOT NULL,
  data_hora TIMESTAMP    CONSTRAINT nn_data_hora_tb_visita NOT NULL,
  ip        INET         CONSTRAINT nn_ip_tb_visita NOT NULL,
  CONSTRAINT pk_codigo_tb_visita PRIMARY KEY (codigo)
);

/* Criação da Função para DML */
CREATE OR REPLACE FUNCTION fn_visita(
  p_codigo   tb_visita.codigo%TYPE,
  p_nome     tb_visita.nome%TYPE,
  p_mensagem tb_visita.mensagem%TYPE,
  p_ip       tb_visita.ip%TYPE,
  operacao   CHAR(1)
)
  RETURNS TEXT AS
  $$
  DECLARE
    v_registro tb_visita%ROWTYPE;
    v_mensagem TEXT;
  BEGIN
    SELECT * INTO v_registro
    FROM tb_visita
    WHERE codigo = p_codigo;

    IF operacao = 'I' THEN
      IF p_codigo IS NULL THEN
        INSERT INTO tb_visita
        (nome, mensagem, data_hora, ip)
        VALUES
        (p_nome, p_mensagem, current_timestamp, p_ip);

        v_mensagem := 'Mensagem gravada com sucesso!';

      ELSE
        v_mensagem := 'O código não deve ser informado para operação INSERT';
      END IF;

    ELSIF operacao = 'U' THEN
        IF v_registro.codigo IS NOT NULL THEN
          UPDATE tb_visita SET
            nome = p_nome, mensagem = p_mensagem, data_hora = current_timestamp
          WHERE codigo = p_codigo;

          v_mensagem := 'Mensagem atualizada com sucesso';

        ELSE
          v_mensagem := 'Registro não encontrado';
        END IF;

    ELSIF operacao = 'D' THEN
        IF v_registro.codigo IS NOT NULL THEN
          DELETE FROM tb_visita
          WHERE codigo = p_codigo;

          v_mensagem := 'Mensagem deletata com sucesso';

        ELSE
          v_mensagem := 'Registro não encontrado';
        END IF;
    ELSE
      v_mensagem := 'Operação '||operacao||' é inválida!';
    END IF;

    RETURN v_mensagem;

  END
  $$ LANGUAGE plpgsql;

/* Criação da View para retorno de data e hora amigável */
CREATE OR REPLACE VIEW vw_visita AS
  SELECT
    codigo,
    nome,
    mensagem,
    TO_CHAR(data_hora, 'TMDay, DD "de" TMMonth "de" YYYY "às" HH24:MI:SS') AS data_hora
  FROM tb_visita
;

/* Cração da Tabele de Log */
CREATE TABLE tb_visita_log(
  cod_visita_log SERIAL,
  usuario name,
  dt_operacao TIMESTAMP,
  operacao CHAR(1),
  codigo    INTEGER,
  nome      VARCHAR(30),
  mensagem  VARCHAR(255),
  data_hora TIMESTAMP,
  ip        INET,  
  CONSTRAINT pk_cod_visita_log_tb_visita_log PRIMARY KEY (cod_visita_log)
);

/* Cração da Trigger para Log */
CREATE OR REPLACE FUNCTION fn_tg_visita()
RETURNS TRIGGER AS
$$
BEGIN
  IF TG_OP = 'INSERT' THEN
    INSERT INTO tb_visita_log (SELECT nextval('tb_visita_log_cod_visita_log_seq'), session_user, current_timestamp, 'I', new.*);

  ELSIF TG_OP = 'UPDATE' THEN
    INSERT INTO tb_visita_log (SELECT nextval('tb_visita_log_cod_visita_log_seq'), session_user, current_timestamp, 'U', new.*);

  ELSIF TG_OP = 'DELETE' THEN
    INSERT INTO tb_visita_log (SELECT nextval('tb_visita_log_cod_visita_log_seq'), session_user, current_timestamp, 'D', old.*);

  END IF;

  RETURN NULL;

END
$$ LANGUAGE plpgsql;

CREATE TRIGGER tg_tb_visita_log
AFTER INSERT OR UPDATE OR DELETE ON tb_visita
FOR EACH ROW EXECUTE PROCEDURE fn_tg_visita();

/* Insert de teste */
--
SELECT fn_visita(
  NULL, 'Vinicius', 'Great work! Congrats!!!! * :)', '127.0.0.1', 'I'
);