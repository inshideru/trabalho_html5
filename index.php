<?php require_once 'app/init.php'; ?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Projeto">
    <meta name="author" content="Vinicius Sorrentino">
    <title>Alan Turing</title>

    <link href="css/font-awesome.min.css" rel="stylesheet">
    <link href="css/projeto.css" rel="stylesheet">

</head>
<body>


<div class="conteudo">
    <header class="clearfix">
        <nav>
            <h3 class="texto-cinza logo pull-left">Alan Turing</h3>
            <ul class="nav nav-pills pull-right">
                <li><a class="botao botao-menu" href="?pagina=home"><i class="fa fa-home"></i> Home</a></li>
                <li><a class="botao botao-menu" href="?pagina=timeline"><i class="fa fa-clock-o"></i> 5 Descobertas</a></li>
                <li><a class="botao botao-menu" href="?pagina=enigma"><i class="fa fa-file-photo-o"></i> Enigma</a></li>
                <li><a class="botao botao-menu" href="?pagina=multimidia"><i class="fa fa-train"></i> Multimídia</a></li>
                <li><a class="botao botao-menu" href="?pagina=guestbook"><i class="fa fa-book"></i> Livro de Visitas</a></li>
            </ul>
        </nav>
        <div class="clearfix"></div>
    </header>
    <!-- Conteúdo variável -->

    <?php $load(); ?>

    <!-- Fim do Conteúdo variável -->
    <footer>
        <p>Todos os direitos reservados &copy; <?php echo date('Y'); ?></p>
    </footer>

</div>
<!-- /conteudo -->

<script src="js/projeto.js"></script>

</body>
</html>