# Projeto de um site simples utilizando HTML5 e PHP

### Recursos utilizados
* Twitter Bootstrap - http://getbootstrap.com/
* Require dinâmico (o conteúdo é 'chamado' através do método get)
* Roteamento básico (redireciona para página de erro caso parâmetro inexistente seja inserido na barra de endereço).

### Instalação e testes
* O arquivo app/database/createDb.sql contém os scripts necessários para criar o banco de dados utilizando PostgreSQL.
* O arquivo app/database/db.php guarda os parâmetros para conexão ao banco de dados. Observar a linha 4, onde está sendo instanciado um objeto da classe PDO.
* Alterar usuário (postgres) e senha (123456) de acordo com o necessário.
