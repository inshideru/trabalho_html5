window.onload = function () {
    var visitMsg = document.getElementById('visit-msg');
    var formTitulo = document.getElementById('form-msg-titulo');
    var confirmDelete = document.getElementById('confirm-delete');
    var inputNome = document.getElementById('nome');
    var inputMensagem = document.getElementById('mensagem');
    var inputCodigo = document.getElementById('codigo');
    var inputOperacao = document.getElementById('operacao');

    var groupNome = document.getElementsByClassName('hide-on-delete')[0];
    var groupMensagem = document.getElementsByClassName('hide-on-delete')[1];

    var botoes_update = document.getElementsByClassName('botao-update');
    var i;
    for (i = 0; i < botoes_update.length; i++) {
        botoes_update[i].onclick = function() {
            formTitulo.innerHTML = '<span class="texto-azul"><i class="fa fa-refresh"></i> Atualizar Mensagem</span>';
            inputNome.value = this.getAttribute('data-nome-message');
            inputMensagem.value = this.getAttribute('data-text-message');
            inputCodigo.value = this.getAttribute('data-id-update');
            inputOperacao.value = 'U';
            confirmDelete.setAttribute('class', 'oculto');
            groupNome.setAttribute('class', 'form-group hide-on-delete');
            groupMensagem.setAttribute('class', 'form-group hide-on-delete');

            window.scrollTo(0, 0);
        };
    }
    var botoes_delete = document.getElementsByClassName('botao-delete');
    for (i = 0; i < botoes_update.length; i++) {
        botoes_delete[i].onclick = function() {
            formTitulo.innerHTML = '<span class="texto-vermelho"><i class="fa fa-trash"></i> Apagar Mensagem</span>';
            inputNome.value = this.getAttribute('data-nome-message');
            inputMensagem.value = this.getAttribute('data-text-message');
            inputCodigo.value = this.getAttribute('data-id-delete');
            inputOperacao.value = 'D';
            groupNome.setAttribute('class', 'oculto');
            groupMensagem.setAttribute('class', 'oculto');

            confirmDelete.setAttribute('class', 'mostrar');
            confirmDelete.innerHTML =
                '<div class="bloco-mensagem">' +
                    '<div class="nome-visitante"><i class="fa fa-user"></i> '+ this.getAttribute('data-nome-message')+'</div>'+
                    '<div class="text-mensagem">"'+ this.getAttribute('data-text-message')+'</div>'+
                '</div>';

            window.scrollTo(0, 0);
        };
    }

    var botoes_emoticons = document.getElementsByClassName('atalho_emoticon');
    for (i = 0; i < botoes_emoticons.length; i++) {
        botoes_emoticons[i].onclick = function () {

            inputMensagem.value = inputMensagem.value + ' ' + this.getAttribute('data-atalho');

            return false;
        }
    }

    document.getElementById('remove-visit-msg').onclick = function() {
        visitMsg.setAttribute('class', 'oculto');
    };
};